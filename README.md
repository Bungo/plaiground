# Plaiground

This is a framework for Multi-Agent Reinforcement Simulations/Games. The goal is to create a Plug-And-Play environment, in which you don't have to make any changes to the reinforcement learning algorithm.

This is a product of my Bachelorthesis and abstracts the environment from the agent. The goal is that you can use each Adapter on each Environment without changing any code in each of both components.

It is currently in development and only turnbased Simulations are tested and stable.