from Plaiground.Templates import Environment, Subscriptionlist
import numpy

class PublicGoodsEnvironment(Environment):

  def required_special_config_keys(self):
    return ["salary", "public_goods_multiplication_factor", "poverty_line"]

  def reset_observations(self, agent_ids):
    gamescore = {}
    for agent_id in agent_ids:
      gamescore[agent_id] = 0
    return {"gamescore":gamescore, "public_goods":0}

  def reset_actions(self):
    possible_actions = []
    for i in range (0, self.configs["salary"] + 1, 1):
      possible_actions.append(i)
    Continuous = {}
    Continuous["low"] = numpy.array([float(min(possible_actions))])
    Continuous["high"] = numpy.array([float(max(possible_actions))])
    actions = {"Continuous":Continuous, "Discrete":possible_actions}
    return actions

  def gamestep(self, actions):
    self.observationdict["public_goods"] = 0
    agent_ids = self.observationdict["gamescore"].keys()
    for action in actions:
      for agent_id in agent_ids:
        if agent_id in action:
          if isinstance(action[agent_id], numpy.ndarray):
            action[agent_id] = action[agent_id][0]
          self.observationdict["gamescore"][agent_id] +=  action[agent_id]
          self.observationdict["public_goods"] += self.configs["salary"] - action[agent_id]
    donation = self.observationdict["public_goods"]
    if donation > 0:
      donation *= self.configs["public_goods_multiplication_factor"]
      public_return =  int(donation / len(agent_ids)) + (donation % len(agent_ids) > 0)
      for agent_id in agent_ids:
        self.observationdict["gamescore"][agent_id] +=  public_return 

class StandardPublicGoodsList(Subscriptionlist):

  def remove_observation_for_single_agent(self, observation, agent_id):
    to_remove = []
    observation["gamescore"] = observation["gamescore"][agent_id]
    return observation

  def parse_environment(self, environment):
    if not hasattr(self, "last_gamescore"):
      self.last_gamescore = {}
      for agent in self.agents:
        self.last_gamescore[agent.id] = 0
    observations = environment["Observation"]
    actions = environment["Actions"]
    running = environment["Running"]
    rewards = {}
    for agent in self.agents:
      rewards[agent.id] = observations["gamescore"][agent.id] - self.last_gamescore[agent.id]
      self.last_gamescore[agent.id] = observations["gamescore"][agent.id]
      if not running and observations["gamescore"][agent.id] < self.environment.configs["poverty_line"]:
        rewards[agent.id] -= 10000000000 
    return observations, actions, rewards, running
    
class EndrewardPublicGoodsList(Subscriptionlist):
  
  def remove_observation_for_single_agent(self, observation, agent_id):
    to_remove = []
    observation["gamescore"] = observation["gamescore"][agent_id]
    return observation

  def parse_environment(self, environment):
    if not hasattr(self, "last_gamescore"):
      self.last_gamescore = {}
      for agent in self.agents:
        self.last_gamescore[agent.id] = 0
    observations = environment["Observation"]
    actions = environment["Actions"]
    running = environment["Running"]
    rewards = {}
    for agent in self.agents:
      rewards[agent.id] = self.last_gamescore[agent.id]
      self.last_gamescore[agent.id] = observations["gamescore"][agent.id]
      if not running and observations["gamescore"][agent.id] < self.environment.configs["poverty_line"]:
        rewards[agent.id] -= 10000000000 
      elif running:
        rewards[agent.id] = 0.0
    return observations, actions, rewards, running