import time
import copy

class Gameover():
  TURNS = 1
  EVENT = 2
  TIME = 3

class Environment():

  def __init__(self, gameover_criteria = Gameover.TURNS, number_of_turns = 1, time_limit_until_gameover_ms = 60000, special_configs_dict = {}, epochs = 1):
    if epochs < 1:
      raise Exception('You have to play at least 1 game. Please change "epochs" parameter in Environment constructor')
    else:
      self.max_epochs = epochs
      self.actual_epoch = 0
    if number_of_turns < 1 and gameover_criteria == Gameover.TURNS:
      raise Exception('You have to play at least 1 Turn. Please change number_of_turns parameter in Environment constructor')
    if time_limit_until_gameover_ms < 1 and gameover_criteria == Gameover.TIME:
      raise Exception('You have to play at least 1 ms. Please change time_limit_until_gameover_ms parameter in Environment constructor.')
    if not isinstance(special_configs_dict, dict):
      raise Exception('Param "special_configs_dict" has to be of type "dict"')
    config_keys = self.required_special_config_keys()
    if not isinstance(config_keys, list):
      raise Exception('You have to return a object of type List in method "special_config_keys()')
    for used_keyword in ["gameover_criteria", "number_of_turns", "time_limit"]:
      if used_keyword in config_keys:
        raise Exception('Your required key ' + used_keyword + ' is used by the framework. Please rename the key in the method "special_config_keys"' )
    for key in config_keys:
      if key not in special_configs_dict.keys():
        raise Exception('Your param "special_configs_dict" misses the required configuration key: ' + str(key))
    self.subscriptionlists = []
    self.create_configs(gameover_criteria, number_of_turns, time_limit_until_gameover_ms / 1000, special_configs_dict)
    self.step = 0

  def update_fetch(self, running):
    fetch = {}
    fetch["Observation"] = self.observationdict
    fetch["Actions"] = self.actiondict
    fetch["Running"] =  running
    return fetch

  def create_configs(self, gameover_criteria, number_of_turns, time_limit, special_configs):
    self.configs = special_configs
    for used_keyword in ["gameover_criteria", "number_of_turns", "time_limit"]:
      if used_keyword in self.configs.keys():
        raise Exception('Your pararm special_configs_dict can not use "'+ used_keyword +' as key. It is already used in the basic configs')
    self.configs["gameover_criteria"] = gameover_criteria
    self.configs["number_of_turns"] = number_of_turns
    self.configs["time_limit"] = time_limit

  def timestep(self):
    actions_list = []
    for sub_list in self.subscriptionlists:
      actions = sub_list.receive_actions()
      if not isinstance(actions, dict):
        raise Exception('Subscriptionlist.receive_actions() needs to return a dictionary containing all actions from all agents in format Adapter.id as key : Action as value') 
      actions_list.append(actions)
    self.gamestep(actions_list)
    running = self.sim_continue()
    self.fetch = self.update_fetch(running)
    for sub_list in self.subscriptionlists:
      sub_list.interpret_environment(self.fetch, False)
    if running:
      self.timestep()

  def turn(self):
    self.step += 1
    self.observationdict["actual_turn"] = self.step + 1
    if self.step < self.configs["number_of_turns"]:
      return True
    else:
      return False

  def time(self):
    self.observationdict["actual_time"] = time.time() - self.timer
    if self.observationdict["actual_time"] < self.configs["time_limit"]:
      return True
    else:
      return False

  def sim_continue(self):
    switcher={
              Gameover.TURNS:self.turn,
              Gameover.TIME:self.time,
              Gameover.EVENT:self.event
            }
    func = switcher.get(self.configs["gameover_criteria"], self.turn)
    running = func()
    if not isinstance(running, bool):
        raise Exception('Gameover-Events need to return a boolean')
    return running

  def add_subscripionlist(self, subscriptions):
    if isinstance(subscriptions, Subscriptionlist):
      self.subscriptionlists.append(subscriptions)
    else:
      raise Exception('subscription is not from type "Plaiground.Subscriptionlist"')

  def start(self):
    self.start_function()
    print("Start next Simulation")
    self.timestep()
    self.actual_epoch += 1
    while self.actual_epoch < self.max_epochs:
      self.restart()
      self.start_function()
      self.timestep()
      self.actual_epoch += 1
    else:
      for list in self.subscriptionlists:
        for agent in list.agents:
          agent.change_trainable(False)
      print("The simulation ended")
  
  def start_function(self):
    self.custom_start()
    agent_ids = []
    for sub_list in self.subscriptionlists:
      for agent in sub_list.agents:
        agent_ids.append(agent.id)
    self.timer = time.time()
    self.reset_variables(agent_ids)
    self.fetch = self.update_fetch(True)
    for sub_list in self.subscriptionlists:
      sub_list.interpret_environment(self.fetch, True)

  def reset_variables(self, agent_ids):
    observationdict = self.reset_observations(agent_ids)
    criteria = self.configs["gameover_criteria"]
    if criteria == Gameover.TURNS:
      observationdict["actual_turn"] = 1
    elif criteria == Gameover.TIME:
      observationdict["actual_time"] = time.time() - self.timer
    actiondict = self.reset_actions()
    if not isinstance(observationdict, dict) or not isinstance(actiondict, dict):
        raise Exception('reset_observations() and reset_actions() need to return a dictionary containing all observations / actions from all agents in format Adapter.id as key : Observation / Action as value')
    else:
      self.observationdict = observationdict
      self.actiondict = actiondict

  def restart(self):
    self.custom_restart()
    self.step = 0
    self.observationdict = None
    self.fetch = None
    self.actiondict = None
    for list in self.subscriptionlists:
      list.reset()
    
  
  def required_special_config_keys(self):
    """Optional: Return a list with selfdefined Keys for special configs. Ensures on Environment-building, that an Exception is thrown, if keys are not defined in param special_configs_dict"""
    return []
  
  def reset_actions(self):
    """Initialize here all environment actions. If you mix discrete and continuous agents, then return a dict containing an Array with all actions for key 'Discrete' and array or dict for key 'Continuous' """
  
  def reset_observations(self, agent_ids):
    """Initialize here all environment observations. agent_ids is a list. Put the variables into a dict with Observationname as key. Use agent_ids only if you have special observations for specific agents (for example Gamescore Agent i)"""
    
  def custom_start(self):
    """Overwrite if you have special startingconditions on environment. This is the first method called in Environment.start()"""

  def custom_restart(self):
    """Overwrite if you have special restartingconditions on environment or if you want to delete some special self.vars. This is the first method called in Environment.restart()"""
    
  def event(self):
    """Overwrite this method, if gameover_criteria is Gameover.EVENT. Use this if the event is something like a special action from an agent or a special value from a variable finishs the game. This mehtod has to return a boolean => True if the game is still running and False if it's Gameover"""      

  def gamestep(self, actions):
    """You get a list filled with Action-Dictionaries. Each dict contains every action from every agent of a specific subscriptionlist. Implement simulationstep / one frame as gamelogic. A gamestep is the same as a new round in Turnbased simulation or a Inputmoment in a continuous simulation. Update all variables inside observationlist after one gamestep! Also if you have a changing actionset or changing configs, update them inside this method"""


class Subscriptionlist():

  def __init__(self, environment):
    self.environment = environment
    self.environment.add_subscripionlist(self)
    self.agents = []

  def subscribe(self, agent):
    self.agents.append(agent)

  def unsubscribe(self, agent):
    if agent in self.agents:
      self.agents.remove(agent)
      return True
    else:
      return False

  def receive_actions(self):
    action_return = {}
    for agent in self.agents:
      action_return[agent.id] = agent.get_action()
    return action_return

  def interpret_environment(self, environment, start):
    observation, actions, rewards, running = self.parse_environment(environment)
    if not isinstance(observation, dict) or not isinstance(actions, dict) or not isinstance(rewards, dict) or not isinstance(running, bool):
      raise Exception("Subscriptionlist.parse_environment returns wrong output. It should return dict, dict, dict and bool")
    for agent in self.agents:
      single_observation = copy.deepcopy(observation)
      single_action = copy.deepcopy(actions)
      single_observation = self.remove_observation_for_single_agent(single_observation, agent.id)
      single_action = self.remove_action_for_single_agent(single_action, agent.id, agent.agent_type)
      if start:
        agent.reset(single_observation, single_action)
      else:   
        agent.push_env_step(single_observation, single_action, rewards[agent.id], running) 
      
        
  def remove_observation_for_single_agent(self, observation, agent_id):
    """If you want to individualize the Observationsreturn for one agent (for example only return own Gamescore) then override this method. It is called after parse_environment and is used individually for each agent"""
    return observation
    
  def remove_action_for_single_agent(self, actions, agent_id, agent_type):
    """If you want to individualize the Actionreturn for one agent (for example only return some actions and not all) then override this method. It is called after parse_environment and is used individually for each agent"""
    if agent_type in actions:
      return actions[agent_type]
    else:
      return actions
   
  def reset(self):
    """Overwrite if you have special resetingconditions on subscriptionlist or if you want to delete some special self.vars"""

  def parse_environment(self, environment):
    """Return the following: observations as a list, actions as a list, rewards as a dict{adapter.id as key:reward as value} and a boolean that indicates, if it's game over for this agent. Calculate Rewards, filter needed observations and needed actions. You don't have to use the whole fetch. You can individualize the given observations and actions for each subscriptionlist."""

class Adapter():

  unique_ids = []

  def __init__(self, agent_class, identifier, agent_params = {}, episodes = True, trainable = True, action_space = "Discrete"):
    if not isinstance(agent_params, dict):
      raise Exception('Adapter.agent_params has to be a dict containing the params value. Use the paramname as key')
    self.agent_class = agent_class
    self.agent_params = agent_params
    self.init = False
    self.trainable = trainable
    self.agent_type = action_space
    if not isinstance(identifier, str):
      raise Exception('Adapter.identifier has to be a string')
    elif len(identifier) <= 0:
      raise Exception('Adapter.identifier needs at least one character')
    elif identifier in Adapter.unique_ids:
      raise Exception('Adapter.identifier needs to be unique. There is already an Adapter with id ' + identifier)
    else:
      self.id = identifier
      Adapter.unique_ids.append("identifier")
    if isinstance(episodes, bool):
      self.episodes = episodes
    else:
      raise Exception('Adapter.episodes needs to be a boolean')

  def get_action(self):
    self.last_action = self.action(self.observation)
    return self.last_action

  def reset(self, observation, actions):
    self.observation = observation
    self.actions = actions
    self.rewards = []
    self.last_action = None
    if not hasattr(self, 'agent'):
      agent = self.build_agent(self.agent_class, self.agent_params, self.shape_actionspace(actions), self.shape_observationspace(observation))
      if isinstance(agent, self.agent_class):
        self.agent = agent
      else:
        raise Exception('Adapter.agent needs to bi of class Adapter.agent_class ... Something went wrong in function Adapter.buld_agent()')
    self.trainable = self.change_trainable(self.trainable)
    if not isinstance(self.trainable, bool):
      raise Exception('return of Adapter.change_trainable has to be a boolean')
    
    self.custom_reset(observation, actions)
  
  def push_env_step(self, observation, actions, reward, running):
    self.rewards.append({"Chosen action":self.last_action, "Observation":copy.deepcopy(self.observation), "Actions":copy.deepcopy(self.actions), "Reward":reward})
    self.custom_step_push(observation, actions, reward, running)
    self.observation = observation
    self.actions = actions
    self.running = running
    if (not self.episodes or not running) and self.trainable:
      self.train()
  
  def custom_step_push(self, observation, actions, reward, running):
    """Is called after saving episode or single-reward. Use this to push Transitions to agents or other custom processes needed by agent"""
  
  def save_agent(self, filename, additional_params):
    """Overwrite this function, if the agent can be saved"""
    print("No save-function is implemented or specified")
    
  def load_agent(self, file, additional_params):
    """Overwrite this function, if the agent can be loaded"""
    print("No load-function is implemented or specified")
  
  def change_trainable(self, trainable_bool):
    """Return true, if agent is trainable and false if not. Will be called with given param constructur-param "trainable" automaticaly, after agent is built (on function "reset"). Use this function to prepare agent and maybe other components for training. Is called with trainable_bool = false after a Environment finished all epochs, to freeze agent. Is maybe also called in other classes, whenever there is a need for a change (for example after a trainingssession)"""
    return trainable_bool
    
  def custom_reset(self, observation, actions):
    """If you want to do something special before the simulation starts (For example initialize the Agent). It will be called at the end of the reset()-Method"""

  def train(self):
    """Use train to train on self.rewards"""

  def action(self, observation):
    """Use Act-Method of agent. Has to return the new action"""  

  def build_agent(self, agent_class, agent_params, actionspace, observationspace):
    """Building the agent with the given agent_params and the given actionspace. Return the invoked model"""    
    return agent_class()
    
  def shape_actionspace(self, actions):
    """Use this function to shape the actionspace in a way, that your agent can actually read it"""
    return len(actions)
    
  def shape_observationspace(self, observations):
    """Use this function to shape the observationspace in a way, that your agent can actually read it"""
    return observations