import numpy as np
from huskarl.memory import Transition
import huskarl as hk
from Plaiground.Templates import Adapter
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import InputLayer

class A2CAdapter(Adapter):
  
  #This function sets in https://github.com/danaugrs/huskarl/blob/master/huskarl/agent/a2c.py defined defaults, if no params are given (except instances).
  def build_agent(self, agent_class, agent_params, actionspace, observationspace):
    if "model" in agent_params.keys():
      temp_model = agent_params["model"]
    else:
      raise Exception('Adapter.agent_params needs at least a param "model" containing a neural network')

    model = Sequential(
        [InputLayer(input_shape = observationspace)]
    )

    for layer in  temp_model.layers:
      model.add(layer)
      
    if "optimizer" in agent_params.keys():
      optimizer = agent_params["optimizer"]
    else:
      optimizer = None

    if "policy" in agent_params.keys():
      policy = agent_params["policy"]
    else:
      policy = [hk.policy.Greedy()] + [hk.policy.GaussianEpsGreedy(eps, 0.1) for eps in np.arange(0, 1, 1)]

    if "test_policy" in agent_params.keys():
      test_policy = agent_params["test_policy"]
    else:
      test_policy = None

    if "gamma" in agent_params.keys():
      gamma = agent_params["gamma"]
    else:
      gamma = 0.99

    if "instances" in agent_params.keys():
      print("Plaiground supports at the moment only Single-Instance A2C. Might be changed in the future. Overwrite this class, if you want to use multiple instances")
    instances = 1

    if "nsteps" in agent_params.keys():
      nsteps = agent_params["nsteps"]
    else:
      nsteps = 1

    if "value_loss" in agent_params.keys():
      value_loss = agent_params["value_loss"]
    else:
      value_loss = 0.5

    if "entropy_loss" in agent_params.keys():
      entropy_loss = agent_params["entropy_loss"]
    else:
      entropy_loss = 0.01

    return agent_class(model, actionspace, optimizer, policy, test_policy, gamma, instances, nsteps, value_loss, entropy_loss)

  def shape_observationspace(self, observations):
    space =  (len(observations.keys()), )
    return space
  
  def change_trainable(self, trainable_bool):
    self.agent.training = trainable_bool
    return trainable_bool

  def save_agent(filename, additional_params):
	  self.agent.model.save_weights(filename, overwrite=additional_params)
  
  def action(self, observation):
    state = []
    for key in sorted(observation.keys()):
      state.append(observation[key])
    return self.agent.act(state)

  def train(self):
    self.agent.train(0) #Param is anyhow never used in Huskarl implementation and therefore not relevant. 0 is just the placeholder
    if not self.running:
      with open(str(self.id) + "train.txt", "a") as myfile:
        myfile.write(str(self.rewards) + "\n")
    
  def custom_step_push(self, observation, actions, reward, running):
    old_state = []
    for key in sorted(self.observation.keys()):
      old_state.append(self.observation[key])
    new_state = []
    for key in sorted(observation.keys()):
      new_state.append(observation[key])
    self.agent.push(Transition(old_state, self.last_action, reward, None if not running else new_state), 0)