import numpy as np
from gym.spaces import Box
from types import SimpleNamespace
from replay_memory import ReplayMemory


from Plaiground.Templates import Adapter

class SACAdapter(Adapter):

  #This function sets in https://github.com/pranz24/pytorch-soft-actor-critic/blob/master/main.py defined defaults, if no params are given.
  #Please import SAC and ReplayMemory from his github or my fork on my gitlab, if you want to use this adapter
  def build_agent(self, agent_class, agent_params, actionspace, observationspace):

    self.actionspace = actionspace
    self.update = 0
    sac_args = SimpleNamespace()

    if "replay_size" in agent_params.keys():
      self.memory = ReplayMemory(agent_params["replay_size"])
    else:
      self.memory = ReplayMemory(1000000)

    if "batch_size" in agent_params.keys():
      self.batch_size = agent_params["batch_size"]
    else:
      self.batch_size = 256

    if "updates_per_step" in agent_params.keys():
      self.updates_per_step = agent_params["updates_per_step"]
    else:
      self.updates_per_step = 1

    self.random = self.updates_per_step > 0

    if "random_steps" in agent_params.keys():
      self.random_steps = agent_params["random_steps"]
    else:
      self.random_steps = 10000

    if "alpha" in agent_params.keys():
      sac_args.alpha = agent_params["alpha"]
    else:
      sac_args.alpha = 0.2

    if "policy" in agent_params.keys():
      sac_args.policy = agent_params["policy"]
    else:
      sac_args.policy = "Gaussian"

    if "target_update_interval" in agent_params.keys():
      sac_args.target_update_interval = agent_params["target_update_interval"]
    else:
      sac_args.target_update_interval = 1

    if "gamma" in agent_params.keys():
      sac_args.gamma = agent_params["gamma"]
    else:
      sac_args.gamma = 0.99

    if "automatic_entropy_tuning" in agent_params.keys():
      sac_args.automatic_entropy_tuning = agent_params["automatic_entropy_tuning"]
    else:
      sac_args.automatic_entropy_tuning = False

    if "cuda" in agent_params.keys():
      sac_args.cuda = agent_params["cuda"]
    else:
      sac_args.cuda = False

    if "hidden_size" in agent_params.keys():
      sac_args.hidden_size = agent_params["hidden_size"]
    else:
      sac_args.hidden_size = 256

    if "tau" in agent_params.keys():
      sac_args.tau = agent_params["tau"]
    else:
      sac_args.tau = 0.005

    if "lr" in agent_params.keys():
      sac_args.lr = agent_params["lr"]
    else:
      sac_args.lr = 0.0003    

    return agent_class(observationspace, actionspace, sac_args)

  def shape_observationspace(self, observations):
    space =  len(observations.keys())
    return space

  def shape_actionspace(self, actions):
    if "low" not in actions or "high" not in actions:
      raise Exception("If you want to use SACAdapter, then make sure that your Actiondict['Continuous'] is a dict with at least the keys 'low' and 'high'.")
    self.low = actions["low"]
    self.high = actions["high"]
    if not np.isscalar(actions["high"]):
      actions["high"] = self.form_actions_to_shape(1, np.array(actions["high"]).shape)
    if not np.isscalar(actions["low"]):
      actions["low"] = self.form_actions_to_shape(-1, np.array(actions["low"]).shape)
    if "dtype" in actions:
      dtype = actions["dtype"]
    else:
      dtype = np.float32
    if "shape" in actions:
      self.shape = actins["shape"]
      space = Box(low=actions["low"], high=actions["high"], shape = actions["shape"], dtype=dtype)
    else:
      self.shape = 0
      space = Box(low=actions["low"], high=actions["high"], dtype = dtype)
    return space

  def change_trainable(self, trainable_bool):
    self.agent.training = trainable_bool
    return trainable_bool

  def save_agent(self, filename, additional_params):
    if "suffix" in additional_params:
      self.agent.save_model(self.id + "_" + filename, additional_params["suffix"], additional_params['actor_path'], additional_params['critic_path'])
    else:
      self.agent.save_model(self.id + "_" + filename, "", additional_params['actor_path'], additional_params['critic_path'])

  def load_agent(self, filename, additional_params):
    self.agent.load_model(self, additional_params['actor_path'], additional_params['critic_path'])
  
  def form_actions_to_shape(self, start, shape):
    for dimension in shape[::-1]:
      build_list = []
      for i in range(dimension):
        build_list.append(start)
      start = build_list
    return np.array(start)
  
  def action(self, observation):
    if self.random_steps > 0:
      self.random_steps -= 1
      self.unscaled = self.actionspace.sample()
      if self.random_steps == 0:
        self.random = False
        print("Finished random sampling")
    else:
      state = []
      for key in sorted(observation.keys()):
        state.append(observation[key]) 
      self.unscaled = self.agent.select_action(state)
    if self.shape != 0 and (np.isscalar(self.high) or np.isscalar(self.low)):
      if np.isscalar(self.high):
        high = self.form_actions_to_shape(self.high, self.shape)
      if np.isscalar(self.low):
        low = self.form_actions_to_shape(self.low, self.shape)
    else:
      low = self.low
      high = self.high
    return (((self.unscaled + 1) / 2) * (high - low)) + low #SAC samples between -1 to 1 and the result has to be scaled to real scale

  def train(self):
    if self.agent.training and len(self.memory) > self.batch_size:
      for i in range(self.updates_per_step):
        critic_1_loss, critic_2_loss, policy_loss, ent_loss, alpha = self.agent.update_parameters(self.memory, self.batch_size, self.update)
        self.update += 1
        if not self.running and not self.random:
          with open(str(self.id) + "train.txt", "a") as myfile:
            myfile.write(str(self.rewards) + "\n")
        
  def custom_step_push(self, observation, actions, reward, running):
    old_state = []
    for key in sorted(self.observation.keys()):
      old_state.append(self.observation[key])
    new_state = []
    for key in sorted(observation.keys()):
      new_state.append(observation[key])
    self.memory.push(old_state, self.unscaled, reward, new_state, float(running))